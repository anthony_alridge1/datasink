from django.apps import AppConfig


class SinkConfig(AppConfig):
    name = 'sink'
