from django.shortcuts import render
#from django.shortcuts import get_object_or_404
#from django.http import HttpResponse
from .models import TrackTerms, Tweets
from . import processor
# Create your views here.


def home(request):
    query_request = request.POST.get('query')
    delete_request = request.POST.get('delete')
    if query_request:
        t = TrackTerms(hashtags=query_request)
        t.save()
        processor.main('#python')
    if delete_request:
        TrackTerms.objects.filter(hashtags=delete_request).delete()
    track_terms = [str(obj) for obj in TrackTerms.objects.all()]
    
    results = Tweets.objects.all().order_by('id').reverse()[:20]
    context = {
        'track_terms': track_terms,
        'results': results}
    return render(request, 'sink/home.html', context)


def results(request):
    return render(request, 'sink/results.html')
