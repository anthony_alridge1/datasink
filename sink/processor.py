#tweepy is th twitter api we'll use
import tweepy
#handles authorisation
from tweepy import OAuthHandler
#allows us to keep a open conection and collect a stream of tweets.
from tweepy import Stream
from tweepy.streaming import StreamListener
#will create seperate thread for streaming and processing
import time
#imports for 'smart' analysis
#from nltk.tag import pos_tag
#import pycountry
#from incf.countryutils import transformations
from .models import Tweets, TrackTerms
#necessary strings to grant authorisation
CONSUMER_KEY = 'xNpLwBql1xxKQFdcI1RavrYv5'
CONSUMER_SECRET = 'Vo14iKAhZJaFR9YuluDrDqQA7PGyljGIsqFo1F2WAkTmeILJOm'
ACCESS_TOKEN = '4889678691-ItpKYP4Ba96F0x9SP9TPyKmNmdlZeCBu57yAg2N'
ACCESS_SECRET = '9u78AM82wLNRWD5O2I2yXU4qtaT6R2MYY3xVxrrcLO5kh'
#completing the authorsation process
auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
api = tweepy.API(auth)


class MyListener(StreamListener):
    def __init__(self):
        self.data_processor = PreProcessing()
        self.exp_count = 0
        self.counter = 0
        self.isReset = False
        self.tweetsAndLocations = {}
        print("CREATING A NEW STREAM OBJECT...")
        print('\n')

    def on_data(self, data):
        try:
                #analysing/cleaning data
                tweet = self.data_processor.findTweet(data)
                location = self.data_processor.findLocation(data)
                # storing the important stuff
                add_to_database = Tweets(tweets=tweet)
                add_to_database.save()

                self.counter += self.data_processor.findHashtags(data, self.isReset)

                if self.counter >= 10000:
                    self.counter = 0
                    self.isReset = True
                    print('OVER 1000 TWEETS HAVE BEEN COLLECTED...')
                    return False

                else:
                    self.isReset = False
                    return True
                
        except BaseException as e:
            print(("Error on_data: %s" % str(e)))
        return True

    def on_error(self, status):
        print(status)
        if status == '420':
            #if a 420 status error is called backoff exponentially
            self.delay = 2 ** self.exp_count
            time.sleep(60 * self.delay)
            self.exp_count += 1
        return True


class PreProcessing():
    '''
    class containing all necessary anclliary functions to help in cleaning
    the data formats. Note that this will be seperate from the PostProcessor class
    which will focus on useful analysis of the data.
    '''
    def __init__(self):
        self.hashtag_counter = 0

    def findTweet(self, data):
        '''
        scans the JSON string for the text field and returns tweet as a string
        '''
        tweet = data.split('"text":')[1].split('","source":')[0]
        return tweet

    def deleteRepeats(self, data):
        '''
        check if text is same as a previous tweet. If so delete it.
        '''
        pass

    def findHashtags(self, data, isReset):
        import re
        '''
        isReset -- boolean object used to determine whether the stream object has been closed and is reconnecting

        Note that this function adds hashtags found within each tweet to the hashtag list, but
        the RETURN OBJECT is an integer counting number of hashtags in the list

        ---ALSO NOTE THAT WE CAN JUST USE LEN FUNCTION IN THE COMPARISON IN ON-DATA METHOD...much more readable, no need for counter!
        '''

        hashtags = re.findall(r"#" + "\w+", data)
        for each_hashtag in hashtags:
            t = TrackTerms(hashtags=each_hashtag)
            t.save()
            self.hashtag_counter += 1
        if isReset:
            self.hashtag_counter = 0
        return self.hashtag_counter

    def findLocation(self, data):
        location = data.split('"location":')[1].split(',"url"')[0]
        return location


class PostProcessing():

    def __init__(self):
        pass

    def rankHashtags(self, hashtagList):
        '''
        Collects most relevant hashtags from list.
        hashtagList --  list object returned from findHashtags containing all hashtags within a given tweet.

        returns a list object with 50 elements.
        '''
        counter = {}
        for word in hashtagList:
            if word in counter:
                counter[word] += 1
            else:
                counter[word] = 1
        orderList = sorted(counter, key=counter.get, reverse=True)
        most_popular = orderList[:50]
        return most_popular


def main(track_these):
    '''
    mainloop function
    track_these -- list object supplied by the user containing a list of items to track
    '''
    try:
        stream = Stream(auth, MyListener())
    except:
        print("Make sure you have an internet conncetion")
    running = True
    streaming = False
    while running:
        if not streaming:
            # try-except here
            try:
                stream.filter(track=track_these, async=True)
            except:
                stream.disconnect()
            streaming = True
        else:
            running = False
            #track_these = PostProcessing().rankHashtags(hashtag_list)
            streaming = False


if __name__ == '__main__':
    main(track_these=None)



