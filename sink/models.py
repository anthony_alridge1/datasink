from django.db import models

# Create your models here.


class TrackTerms(models.Model):
    hashtags = models.CharField(max_length=30)

    def __str__(self):
        return self.hashtags


class Tweets(models.Model):

    tweets = models.CharField(max_length=100)

    def __str__(self):
        return self.tweets