from django.conf.urls import url
from . import views

app_name = 'sink'

urlpatterns = [
    url(r'^results$', views.results, name='results'),
    url(r'^$', views.home, name='home'),




]
